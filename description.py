﻿# -*- coding: utf-8 -*-
from vkbot import VK, default_logging
import ConfigParser
import logging
import time
logging.basicConfig(level=logging.DEBUG)
from settings import login, password, descriptions_in, sleep, description_options

def main():
    default_logging()
    descriptions = [description.strip() for description in open(descriptions_in)]

    vk = VK(login, password)
    vk.log_in()
    while descriptions:
        description = descriptions.pop()

        fl = open(descriptions_in, "w")
        fl.write('\n'.join(descriptions))
        fl.close()

        group, avatar_path, description_path, photo_path = description.split(';')
        text = open(description_path).read()

        vk.upload_avatar(group, avatar_path)
        vk.describe_group(group, text.decode('utf8'), description_options)
        for line in open(photo_path):
            vk.upload_photo(group, line.strip())
            time.sleep(3)
        time.sleep(sleep)
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print "Script is stopped"