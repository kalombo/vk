﻿# -*- coding: utf-8 -*-
from vkbot import VK, default_logging
import ConfigParser
import logging
import time
logging.basicConfig(level=logging.DEBUG)
from settings import names_in, out, login, password, sleep

def main():
    default_logging()
    names = [name.strip() for name in open(names_in)]
    out_file = open(out, "a")

    vk = VK(login, password)
    vk.log_in()
    while names:
        name = names.pop()
        fl = open(names_in, "w")
        fl.write('\n'.join(names))
        fl.close()
        url = vk.create_group(name.decode('utf8'))
        if url:
            out_file.write(url+"\n")
            out_file.flush()
        time.sleep(sleep)
    out_file.close()
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print "Script is stopped"