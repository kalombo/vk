﻿# -*- coding: utf-8 -*-
from vkbot import VK, default_logging
import ConfigParser
import logging
import time
logging.basicConfig(level=logging.DEBUG)
from settings import login, password, groups_in, sleep, stat_options

def main():
    default_logging()
    groups = [group.strip() for group in open(groups_in)]

    vk = VK(login, password)
    vk.log_in()
    for group in groups:
        url = group
        name = 'stats/' + url.split('/')[-1] + '.txt'
        content = vk.get_stats(group, stat_options)
        fl = open(name, 'w')
        fl.write(content)
        fl.close()
        time.sleep(sleep)
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print "Script is stopped"