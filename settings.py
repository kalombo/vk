﻿# -*- coding: utf-8 -*-

# Общие настройки
login = '89253646714'
password = 'iserver2'
sleep = 5

#Создание групп
names_in = 'data/names.txt'
out = 'groups.txt'


#Загрузка описания
#Группа;путь до файла с аватаром;путь до файла с описанием;путь до файла с путями до файлов с фото
descriptions_in = 'data/descriptions.txt'
# Стену закрывать здесь
description_options = { 'access':'0', 'al':'1', 'audio':'0', 'category':'0',
                        'docs':'0', 'photos':'1', 'subcategory':'0',
                        'topics':'0', 'video':'0', 'wall':'1', 'wiki':'0' }

#Постинг сообщения на стену
#Группа;путь до файла с сообщением;путь до файла с фото
#Если фото не нужно, оставляем пустым, но ";" должно быть две!
messages_in = 'data/messages.txt'

#Сбор статистики
groups_in = 'groups.txt'
stat_options = {'format':'1', 'check':'0',
        'date_from_day':'2', 'date_from_month':'3',
        'date_from_year':'2013', 'date_to_day':'6',
        'date_to_month':'3', 'date_to_year':'2013',
        'data_types':'0,1,2,3,4,5,6'}

#Очистка забаненных
clear_groups_in = 'groups.txt'
