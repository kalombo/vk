﻿# -*- coding: utf-8 -*-
from vkbot import VK, default_logging
import ConfigParser
import logging
import time
logging.basicConfig(level=logging.DEBUG)
from settings import login, password, messages_in, sleep

def main():
    default_logging()
    messages = [message.strip() for message in open(messages_in)]

    vk = VK(login, password)
    vk.log_in()
    while messages:
        message = messages.pop()

        fl = open(messages_in, "w")
        fl.write('\n'.join(messages))
        fl.close()

        group, message_path, photo_path = message.split(';')
        if not photo_path: photo_path = None
        text = open(message_path).read()
        vk.wall_post(group, text.decode('utf8'), photo_path )
        time.sleep(sleep)
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print "Script is stopped"