﻿# -*- coding: utf-8 -*-
from vkbot import VK, default_logging
import ConfigParser
import logging
import time
logging.basicConfig(level=logging.DEBUG)
from settings import login, password, clear_groups_in, sleep

def main():
    default_logging()
    groups = [group.strip() for group in open(clear_groups_in)]

    vk = VK(login, password)
    vk.log_in()
    for group in groups:
        url = group
        vk.del_banned_users(group)
        time.sleep(sleep)
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print "Script is stopped"