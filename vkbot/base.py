﻿# -*- coding: utf-8 -*-
from grab import Grab, DataNotFound, UploadFile
import logging
import error
import time
import re
import json
import urllib
from urlparse import urljoin

logger = logging.getLogger(__name__)

def default_logging(vk_log='log/vk.log', level=logging.DEBUG, mode='w'):
    """
    Настройка логирования в файл.
    """

    vk_logger = logging.getLogger('vkbot.base')
    hdl1 = logging.FileHandler(vk_log, mode)
    vk_logger.addHandler(hdl1)
    vk_logger.setLevel(level)

def login_required(method):
    def wrapper(self, *args, **kwargs):
        if not self.is_logged:
            raise error.VkError('Login required: account is not logged')
        return method(self, *args, **kwargs)
    return wrapper

class VK(object):
    def __init__(self, login, password):
        self.login = login
        self.password = password
        self.grab = Grab(log_dir='log', debug_post=True)
        self.is_logged = False

    def log_in(self):
        """
        Логин в аккаунт, возвращает True или False
        """

        logger.debug("Try to login.")
        url = "http://vk.com/login.php"
        self.grab.go(url)

        self.grab.set_input('email', self.login)
        self.grab.set_input('pass', self.password)
        self.grab.submit()

        if self.grab.search(u'act=logout'):
            logger.info("Account is logged")
            self.is_logged = True
        else:
            logger.info("Account is not logged")
            self.is_logged = False
        return self.is_logged

    @login_required
    def create_group(self, title):
        '''
        Создание группы, название группы в unicode
        '''

        logger.debug('Try to create group')
        url = "http://vk.com/groups"
        self.grab.go(url)

        url = "http://vk.com/al_groups.php"
        data = "act=create_box&al=1"
        self.grab.setup(post=data)
        self.grab.go(url)

        try:
            hsh = self.grab.rex_text("hash: '(.*?)'")
            logger.debug('Find hash %s' % hsh)
        except DataNotFound:
            logger.debug('Could not find hash')
            return None

        url = "http://vk.com/al_groups.php"
        data = {'act':'create', 'al':'1', 'hash':hsh, 'title':title.encode('utf8')}
        self.grab.setup(post=data)
        self.grab.go(url)

        try:
            url = "http://vk.com" + self.grab.rex_text('(/club.*?)\?act=edit_first')
            logger.info("Create group %s" % url)
            return url
        except DataNotFound:
            logger.info("Could not create group")
            return None

    @login_required
    def describe_group(self, group, text, options={}):
        '''
        Замена описания группы, text - unicode
        '''

        if not group.endswith('?act=edit'):
            group += "?act=edit"
        self.grab.go(group)

        addr = group.replace('http://vk.com/', '').replace('?act=edit', '')
        try:
            name = self.grab.xpath_one('//input[@id="group_edit_name"]/@value')
            gid = self.grab.rex_text('gid: (\d+)')
            hsh = self.grab.rex_text("hash: '(.*?)'")
        except DataNotFound:
            logger.debug('Could not change description')
            return None

        url = "http://vk.com/al_groups.php"
        data = {'access':'0', 'act': 'save', 'addr': addr, 'al':'1',
                'audio':'0', 'category':'0', 'description': text.encode('utf8'), 'docs':'0',
                'gid':gid, 'hash':hsh, 'name':name.encode('utf8'), 'photos':'1', 'rss':'',
                'subcategory':'0', 'topics':'0', 'video':'0', 'wall':'1',
                'website':'', 'wiki':'0'}
        data.update(options)
        self.grab.setup(post=data)
        self.grab.go(url)
        if self.grab.search(u'>club'):
            logger.info('Change group description')
            return True
        else:
            logger.debug('Could not change description')

    @login_required
    def upload_avatar(self, group, path, size='5,5,200,200'):
        '''
        Смена аватара группы
        '''

        group = group.replace('?act=edit', '')
        self.grab.go(group)

        try:
            oid = self.grab.rex_text('"wall_oid":(.*?),')
        except DataNotFound:
            logger.debug('Could not upload avatar')
            return None

        url = 'http://vk.com/al_page.php'
        data = { "act":'owner_photo_box', 'al':'1', 'oid':oid }
        self.grab.setup(post=data)
        self.grab.go(url)

        try:
            upload_url = self.grab.rex_text('"url":"(http.*?)"').replace("\\", '')
            check_url = self.grab.rex_text('"check_url":"(http.*?)"').replace("\\", '')
        except DataNotFound:
            logger.debug('Could not upload avatar')
            return None

        url = check_url
        data = {'_jsonp':'3'}
        self.grab.setup(multipart_post=data)
        self.grab.go(url)

        try:
            query = self.grab.rex_text('''\('({"success"[^)]+})'\);''')
        except DataNotFound:
            logger.debug('Could not upload avatar')
            return None

        url = "http://vk.com/upload_fails.php"
        data = { "_query":query, 'act':'check_result', 'al':'1' }
        self.grab.setup(post=data)
        self.grab.go(url)

        url = upload_url
        data = {'photo': UploadFile(path)}
        self.grab.setup(multipart_post=data)
        self.grab.go(url)

        query = self.grab.response.body

        link = '/upload.php?act=owner_photo_edit&_query={0}'\
                '&_origin=http%3A%2F%2Fvk.com&_full={1}&_rot=0'\
                '&_crop=0,0,200&_jsonp=8&_origin=http%3A%2F%2Fvk.com'.\
                format(urllib.quote_plus(query), urllib.quote_plus(size))
        url = urljoin(url, link)
        self.grab.go(url)

        try:
            query = self.grab.rex_text('''\('({"oid"[^)]+})'\);''')
        except DataNotFound:
            logger.debug('Could not upload avatar')
            return None
        url = 'http://vk.com/al_page.php'
        data = { "_query":query, "act":'owner_photo_save', 'al':'1' }
        self.grab.setup(post=data)
        self.grab.go(url)
        if self.grab.search(u'>club'):
            logger.info('Change group description')
            return True
        else:
            logger.debug('Could not change description')

    @login_required
    def wall_post(self, group, message, photo=None, options={}):
        group = group.replace('?act=edit', '')
        self.grab.go(group)

        try:
            hsh = self.grab.rex_text('"post_hash":"(.*?)"')
            to_id = self.grab.rex_text('"wall_oid":(.*?),')
        except DataNotFound:
            logger.debug('Could not change description')
            return None

        main_url = 'http://vk.com/al_wall.php'
        main_data = { 'Message': message.encode('utf8'),
                 'act':	'post', 'al':'1', 'facebook_export':'',
                 'fixed': '', 'friends_only':'', 'from':'',
                  'hash': hsh, 'official': '', 'signed': '',
                  'status_export':'', 'to_id': to_id, 'type': 'all'}
        main_data.update(options)
        if photo is not None:
            url = "http://vk.com/al_photos.php"
            data = { 'act':'choose_photo', 'al':'1', 'mail_add':'',
                     'scrollbar_width':'12', 'to_id':to_id }
            self.grab.setup(post=data)
            self.grab.go(url)
            try:
                upload_url = self.grab.rex_text("cur\.html5LiteUrl = '(http.*?)'")
                mid = self.grab.rex_text('"mid":(.*?),')
                aid = self.grab.rex_text('"aid":(.*?),')
                gid = self.grab.rex_text('"gid":(.*?),')
                hsh = self.grab.rex_text('"hash":"(.*?)"')
                rhash = self.grab.rex_text('"rhash":"(.*?)"')
                server = self.grab.rex_text('server: (.*?),')
            except DataNotFound:
                logger.debug('Could not wall post with photo')
                return None

            url = upload_url
            data = { 'mid':mid, 'aid':aid, 'gid':gid,
                 'hash':hsh, 'rhash':rhash,
                  'al':'1', 'act':'check_upload', 'type':'photo',
                  'ondone':'Upload.callbacks.oncheck0'}
            self.grab.setup(multipart_post=data)
            self.grab.go(url)

            url = upload_url
            data = {  'aid':aid, 'gid':gid, 'mid':mid,
                'hash':hsh, 'rhash':rhash, 'act':'do_add', 'jpeg_quality':'89',
                 'ajx':'1', 'vk':'1', 'from_host':'vk.com' }
            url += "?" + urllib.urlencode(data)
            data = {'photo': UploadFile(photo)}
            self.grab.setup(multipart_post=data)
            self.grab.go(url)

            try:
                mid = self.grab.rex_text('"mid":(.*?),')
                aid = self.grab.rex_text('"aid":(.*?),')
                gid = self.grab.rex_text('"gid":(.*?),')
                hsh = self.grab.rex_text('"hash":"(.*?)"')
                server = self.grab.rex_text('"server":(.*?),')
                photos = self.grab.rex_text('(\[\{(.*?)\}\])').replace('\\', '')
            except DataNotFound:
                logger.debug('Could not wall post with photo')
                return None

            url = "http://vk.com/al_photos.php"
            data = { "act": "choose_uploaded", 'aid':aid, 'al':'1',
                    'gid':gid, 'hash':hsh, 'mid':mid, 'photos':photos,
                    'server':server}
            self.grab.setup(post=data)
            self.grab.go(url)
            try:
                photo_id = self.grab.rex_text('>(\d+_\d+)<')
            except DataNotFound:
                logger.debug('Could not wall post with photo')
                return None
            main_data['attach1'] = photo_id
            main_data['attach1_type'] = 'photo'

        self.grab.setup(post=main_data)
        self.grab.go(main_url)
        logger.info('Add wall post')
        return True

    @login_required
    def upload_photo(self, group, path):
        group = group.replace('?act=edit', '')
        self.grab.go(group)

        try:
             url = self.grab.rex_text('<a href="(/album\-.*?)"')
        except DataNotFound:
            logger.debug('Could not post photo')
            return None

        self.grab.go(url)
        try:
            upload_url = self.grab.rex_text("cur\.html5LiteUrl = '(http.*?)'")
            mid = self.grab.rex_text('"mid":(.*?),')
            aid = self.grab.rex_text('"aid":(.*?),')
            gid = self.grab.rex_text('"gid":(.*?),')
            hsh = self.grab.rex_text('"hash":"(.*?)"')
            rhash = self.grab.rex_text('"rhash":"(.*?)"')
            oid = self.grab.rex_text('"oid":(.*?),')
            server = self.grab.rex_text('server: (.*?),')
            stats_hash = self.grab.rex_text("statsPhotoAddHash: '(.*?)'")
        except DataNotFound:
            logger.debug('Could not post photo')
            return None

        url = upload_url
        data = { 'mid':mid, 'aid':aid, 'gid':gid,
                 'hash':hsh, 'rhash':rhash,
                  'al':'1', 'act':'check_upload', 'type':'photo',
                  'ondone':'Upload.callbacks.oncheck0'}
        self.grab.setup(post=data)
        self.grab.go(url)

        url = 'http://vk.com/al_photos.php'
        data = { 'act':	'start_add', 'al': '1', 'hash': stats_hash}
        self.grab.setup(post=data)
        self.grab.go(url)

        url = upload_url
        data = { 'oid':oid, 'aid':aid, 'gid':gid, 'mid':mid,
                'hash':hsh, 'rhash':rhash, 'act':'do_add', 'jpeg_quality':'89',
                'max_files':'200', 'ajx':'1' }
        url += "?" + urllib.urlencode(data)
        data = {'photo': UploadFile(path)}
        self.grab.setup(multipart_post=data)
        self.grab.go(url)

        try:
            mid = self.grab.rex_text('"mid":(.*?),')
            aid = self.grab.rex_text('"aid":(.*?),')
            gid = self.grab.rex_text('"gid":(.*?),')
            hsh = self.grab.rex_text('"hash":"(.*?)"')
            server = self.grab.rex_text('"server":(.*?),')
            photos = self.grab.rex_text('(\[\{(.*?)\}\])').replace('\\', '')
        except DataNotFound:
            logger.debug('Could not post photo')
            return None

        url = "http://vk.com/al_photos.php"
        data = { "act": "done_add", 'aid':aid, 'al':'1', 'context':'1',
                'from':'html5', 'geo':'1', 'gid':gid, 'hash':hsh,
                'mid':mid, 'photos':photos, 'server':server}
        self.grab.setup(post=data)
        self.grab.go(url)
        if self.grab.search(u'photos_add_s_thumb'):
            logger.info('Add wall photo')
            return True

    @login_required
    def get_stats(self, group, options={}):
        url = group.replace('club', 'stats?gid=')
        self.grab.go(url)


        try:
            cid = self.grab.rex_text('cid: (\-.*?)\}')
        except DataNotFound:
            logger.debug('Could not get stats')
            return None

        url = "http://vk.com/al_stats.php"
        data = { 'act': 'export_stats_box', 'al':'1', 'cid':cid}
        self.grab.setup(post=data)
        self.grab.go(url)

        try:
            hsh = self.grab.rex_text('name="hash" value="(.*?)"')
        except DataNotFound:
            logger.debug('Could not get stats')
            return None

        url = 'http://vk.com/al_stats.php?act=export_stats'
        data = {'cid':cid, 'hash':hsh, 'format':'1', 'check':'0',
                'date_from_day':'1', 'date_from_month':'3',
                'date_from_year':'2013', 'date_to_day':'5',
                'date_to_month':'3', 'date_to_year':'2013',
                'data_types':'0,1,2,3,4,5,6'}
        data.update(options)
        self.grab.setup(post=data)
        self.grab.go(url)
        return self.grab.response.body

    def del_banned_users(self, group):
        if not group.endswith('?act=users'):
            group += "?act=users"
        self.grab.go(group)
        try:
            groupid = self.grab.rex_text('gid=(\d+)')
        except DataNotFound:
            logger.debug('Could not remove users')
            return None

        url = "https://vk.com/groupsedit.php"
        data = { "act":"get_list", "al":"1", "id": groupid, "tab":"members" }
        self.grab.setup(post=data)
        self.grab.go(url)
        try:
            json_list = self.grab.rex_text('(\[.*\])')
        except DataNotFound:
            logger.debug('Could not remove users')
            return None
        for user in json.loads(json_list):
            addr = str(user[0])
            hsh = str(user[-1])
            image = str(user[3])

            if image == "/images/deactivated_100.gif":
                url = "http://vk.com/groupsedit.php"
                data = { 'act':'user_action', 'action':'-1',
                     'addr':addr, 'al':'1', 'hash':hsh,
                    'id':groupid }
                self.grab.setup(post=data)
                self.grab.go(url)
                logger.info('Remove user %s' % addr)
                time.sleep(5)
